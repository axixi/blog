export default class XTextProgress extends HTMLElement {
  static observedAttributes = ['done', 'total']

  #$shadow
  #$style = document.createElement('style')
  #$span = document.createElement('span')
  #done
  #total

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: inline-block;
      }
      .x-text-progress {
        display: inline-block;
        background-color: gainsboro;
        border-radius: 4px;
        padding: 0 4px;
        font-size: 14px;
        color: black;
      }
      @media (prefers-color-scheme: dark) {
        .x-text-progress {
          background-color: gray;
          color: white;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$span.classList.add('x-text-progress')
    this.#$shadow.appendChild(this.#$span)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'done') {
      this.#done = newValue
      this.#$span.textContent = `${newValue}/${this.#total}`
    }
    if (name === 'total') {
      this.#total = newValue
      this.#$span.textContent = `${this.#done}/${newValue}`
    }
  }
}

customElements.define('x-text-progress', XTextProgress)
