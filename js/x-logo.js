export default class XLogo extends HTMLElement {
  static observedAttributes = ['size']

  #$shadow
  #$style = document.createElement('style')
  #$a = document.createElement('a')
  #$img = document.createElement('img')

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: inline-block;
        width: fit-content;
      }
      .x-logo {
        display: inline-block;
        font-size: 0;
        padding-bottom: 1px;
        border-bottom: 2px dashed DarkSlateGray;
      }
      .x-logo-large {
        width: 280px;
      }
      .x-logo-small {
        width: 100px;
      }
      @media (prefers-color-scheme: dark) {
        .x-logo {
          border-color: LightGray;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$img.src = '/assets/scbqb-logo.png'
    this.#$img.alt = 'homepage logo'
    this.#$a.href="/"
    this.#$a.appendChild(this.#$img)
    this.#$a.classList.add('x-logo')
    this.#$shadow.appendChild(this.#$a)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'size') {
      this.#$img.classList.remove(`x-logo-${oldValue}`)
      this.#$img.classList.add(`x-logo-${newValue}`)
    }
  }
}

customElements.define('x-logo', XLogo)
