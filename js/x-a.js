export default class XA extends HTMLElement {
  static observedAttributes = ['text', 'href']

  #$shadow
  #$style = document.createElement('style')
  #$a = document.createElement('a')

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: inline-block;
      }
      .x-a {
        display: inline-block;
        font-size: 16px;
        font-weight: 400;
        line-height: 24px;
        color: steelblue;
        text-decoration: none;
        border-bottom: 1px dashed DarkSlateGray;
      }
      .x-a::before {
        content: '';
        display: inline-block;
        width: 16px;
        height: 16px;
        background-color: steelblue;
        mask-image: url(/assets/link.svg);
        vertical-align: -2px;
        margin-right: 4px;
      }
      .x-a:visited {
        color: steelblue;
      }
      @media (prefers-color-scheme: dark) {
        .x-a {
          color: lightsteelblue;
          border-color: LightGray;
        }
        .x-a:visited {
          color: lightsteelblue;
        }
        .x-a::before {
          background-color: lightsteelblue;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$a.classList.add('x-a')
    this.#$shadow.appendChild(this.#$a)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'text') {
      this.#$a.textContent = newValue
    }
    if (name === 'href') {
      this.#$a.setAttribute('href', newValue)
    }
  }
}

customElements.define('x-a', XA)
