export default class XH extends HTMLElement {
  static observedAttributes = ['h', 'text']

  #$shadow
  #$style = document.createElement('style')
  #$span = document.createElement('span')

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: block;
        margin: 12px 0;
      }
      .x-h1 {
        font-size: 34px;
        font-weight: 800;
        line-height: 40px;
      }
      .x-h2 {
        font-size: 26px;
        font-weight: 800;
        line-height: 30px;
      }
      .x-h3 {
        font-size: 22px;
        font-weight: 800;
        line-height: 26px;
      }
      @media (prefers-color-scheme: dark) {
        .x-h1,
        .x-h2,
        .x-h3 {
          color: white;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$shadow.appendChild(this.#$span)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'h') {
      this.#$span.classList.remove('x-h1', 'x-h2', 'x-h3')
      this.#$span.classList.add(`x-h${newValue}`)
    }
    if (name === 'text') {
      this.#$span.textContent = newValue
    }
  }
}

customElements.define('x-h', XH)
