export default class XCheck extends HTMLElement {
  static observedAttributes = ['checked']

  #$shadow
  #$style = document.createElement('style')
  #$span = document.createElement('span')

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: inline-block;
      }
      .x-check {
        display: inline-block;
        width: 100%;
        height: 100%;
        background-color: silver;
        mask-image: url(/assets/check.svg);
      }
      .x-check.checked {
        background-color: seagreen;
      }
      @media (prefers-color-scheme: dark) {
        .x-check {
          background-color: gray;
        }
        .x-check.checked {
          background-color: yellowgreen;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$span.classList.add('x-check')
    this.#$shadow.appendChild(this.#$span)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'checked') {
      if (newValue === 'false') {
        this.#$span.classList.remove('checked')
      } else {
        this.#$span.classList.add('checked')
      }
    }
  }
}

customElements.define('x-check', XCheck)
