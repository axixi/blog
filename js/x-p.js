export default class XP extends HTMLElement {
  static observedAttributes = ['text']

  #$shadow
  #$style = document.createElement('style')
  #$span = document.createElement('span')

  constructor() {
    super()
  }

  connectedCallback() {
    this.#$shadow = this.attachShadow({ mode: 'open' })
    this.#$style.textContent = `
      :host {
        display: block;
        margin: 8px 0;
      }
      .x-p {
        display: inline-block;
        font-size: 16px;
        font-weight: 400;
        line-height: 24px;
      }
      @media (prefers-color-scheme: dark) {
        .x-p {
          color: white;
        }
      }
    `;
    this.#$shadow.appendChild(this.#$style)
    this.#$span.classList.add('x-p')
    this.#$shadow.appendChild(this.#$span)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'text') {
      this.#$span.textContent = newValue
    }
  }
}

customElements.define('x-p', XP)
